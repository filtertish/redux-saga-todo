import { takeEvery, all, call, put } from 'redux-saga/effects';
import axios from 'axios';

import * as actionTypes from './constants';

export function* fetchTodoList() {
  try {
    const { data: todoList } = yield call(
      axios.get,
      `http://localhost:3001/todoList`,
    );
    yield put({ type: 'FETCH_TODO_LIST', todoList })
  } catch {
    console.log('error');
  }
}

function* watchTodo() {
  yield all([
    takeEvery(actionTypes.FETCH_TODO_LIST, fetchTodoList)
  ])
}

export default watchTodo;