import * as actionType from './constants';

const initialState = {
  todoList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FETCH_TODO_LIST:
      return {
        ...state,
        todoList: action.todoList
      };
    default: return {
      ...state
    }
  }
};

export default reducer;