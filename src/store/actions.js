import * as actionTypes from './constants'

const fetchTodoList = () => {
  return {
    type: actionTypes.FETCH_TODO_LIST
  }
};

export const todoActions = {
  fetchTodoList
};