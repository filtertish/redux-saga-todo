import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';

import TodoList from "./components/TodoList";

import reducer from './store/reducers'
import watchTodo from './store/sagas'

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watchTodo);

const app = (
  <Provider store={store}>
    <TodoList />
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

