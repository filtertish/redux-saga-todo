import React, { useEffect, useState } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import TodoItems from "./TodoItems";
import { todoActions } from '../../store/actions'

const TodoList = ({ actions, todoList }) => {
  const [todo, setTodo] = useState('');

  useEffect(() => {
    actions.fetchTodoList()
  }, [actions]);

  return (
    <div>
      <input />
      <button>add</button>
      <TodoItems todoList={todoList}/>
    </div>
  );
};

export default connect(
  state => ({
    todoList: state.todoList,
  }),
  dispatch => ({
    actions: bindActionCreators(
      {
        ...todoActions,
      },
      dispatch,
    ),
  }),
)(TodoList)
